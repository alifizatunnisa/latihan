// Online C++ compiler to run C++ program online
#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int suhuawal=0;
    int step=0;
    int suhuakhir=0;
    
    cout << "Suhu awal : ";
    cin >> suhuawal;
    cout << "Step : ";
    cin >> step;
    cout << "Suhu akhir : ";
    cin >> suhuakhir;
    
    cout<<"----------------------------------------"<<endl;
    cout<<"|  Celcius\t |  Fahrenheit\t |  Kelvin |"<<endl;
    cout<<"----------------------------------------"<<endl;
    for(int celcius = suhuawal;celcius<=suhuakhir;celcius+=step){
        double fahrenheit=(celcius*9/5)+32;
        double kelvin=celcius + 273.15;
       cout << "|"<< setw(7)<<celcius << "     | " << setw(10) <<fixed<< setprecision(2) << fahrenheit << "    | " <<setw(7) << kelvin << " | " << endl;
    }
    cout<<"----------------------------------------"<<endl;

    return 0;
}
