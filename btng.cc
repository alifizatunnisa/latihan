/*
   Nama Program : bintang.cc
   Tgl buat     : 7 November 2023
   Deskripsi    : mencetak bintang
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int N = 0;  // Number of rows

    cout << "Masukkan jumlah awal bintang : ";
    cin >> N;

    for (int i = 1; i <= N; i++) {
        for (int j = 1; j <= i; j++) {
            cout << "*";
        }
        cout << endl;
    } 
    for (int i = N-1; i >= 0; i--) {
        for (int j = 1; j <= i; j++) {
            cout << "*";
        }
        cout << endl;
    } 

  return 0;
}


