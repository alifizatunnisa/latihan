#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
    system("clear");

    int JumlahAnak = 0;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0, GajiBersih = 0.0;

    PersenTunjangan = 0.0;
    cout << "Gaji Kotor ? "; cin >> GajiKotor;
    cout << "Jumlah Anak ? "; cin >> JumlahAnak;
    if (JumlahAnak > 2)
    {
        PersenTunjangan = 0.3;
    }

    Tunjangan = PersenTunjangan * GajiKotor;
    GajiBersih = GajiKotor + Tunjangan;
    cout << "Besar Tunjangan = Rp " << setprecision(2) << Tunjangan << endl;
    cout << "Gaji Bersih = Rp " << setprecision(2) << GajiBersih << endl;

    return 0;
}
