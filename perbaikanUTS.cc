#include <iostream>

using namespace std;

int main() {
    // Input dari pengguna
    float angka1, angka2;
    char operatorMatematika;

    cout << "Masukkan angka pertama: ";
    cin >> angka1;
    
    cout << "Masukkan operator (+, -, *, /): ";
    cin >> operatorMatematika;

    cout << "Masukkan angka kedua: ";
    cin >> angka2;
    
    cout << "Bilangan ke 1 + bilangan ke 2 = 5 + 7";
    // Melakukan operasi sesuai dengan operator
    switch (operatorMatematika) {
        case '+':
            cout << "Hasil: " << angka1 + angka2;
            break;
        case '-':
            cout << "Hasil: " << angka1 - angka2;
            break;
        case '*':
            cout << "Hasil: " << angka1 * angka2;
            break;
        case '/':
            if (angka2 != 0) {
                cout << "Hasil: " << angka1 / angka2;
            } else {
                cout << "Tidak bisa dibagi oleh nol";
            }
            break;
        default:
            cout << "Operator tidak valid";
    }

    return 0;
}
